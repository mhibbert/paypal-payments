from django.shortcuts import render
from products.models import Product


def all_products(request):
    return render(request, 'products.html', { 'products': Product.objects.all() })
