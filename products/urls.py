from django.conf.urls import include, url
from views import all_products

urlpatterns = [
    url(r'^$', all_products, name="all_products"),
]